﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEB_2;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProjectASEB_B
{
    [TestClass]
    public class PartBUnitTest //Unit Test Class
    {
        //Instantiate calc class
        Calc calc = new Calc();

        //Test data
        List<int> numbers = new List<int>();
        int[] testCollection = { 2, 3, 4, 5, 6, 3, 3, 2, 1, 5 };
        double expectedAverageNumber = 3; //3.4 but rounded up
        int expectedMaximumNumber = 6;
        int expectedMinimumNumber = 1;
        double one = 100;
        double two = 200;
        double three = 300;
        double expectedNP = 239;

        //Test for average method
        [TestMethod]
        public void TestCalculateAverageMethod()
        {
            numbers.AddRange(testCollection);
            double results = calc.calculateAverage(numbers);
            Assert.AreEqual(expectedAverageNumber, results);
        }

        //Test for the highest number in an array 
        [TestMethod]
        public void TestCalculateMaximumMethod()
        {
            numbers.AddRange(testCollection);
            double results = calc.calculateMaximum(numbers);
            Assert.AreEqual(expectedMaximumNumber, results);
        }

        //Test for the lowest number in an array 
        [TestMethod]
        public void TestCalculateMinimumMethod()
        {
            numbers.AddRange(testCollection);
            double results = calc.calculateMinimum(numbers);
            Assert.AreEqual(expectedMinimumNumber, results);
        }

        //Test for the calculation of NP (Normalized power).
        [TestMethod]
        public void TestCalculateNPMethod()
        {
            double results = calc.calculateNP(one, two, three);
            Assert.AreEqual(expectedNP, results);
        }

    }
}
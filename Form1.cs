﻿//Program to read data from HRM file and output results using ZedGraph - Created by Mark Ellis : c3374267
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Globalization;
using ZedGraph;

namespace ASEB_2
{
    public partial class Form1 : Form
    {
        //Static variables for totals
        public static double speedTotal;
        public static int heartTotal;
        public static int altitudeTotal;
        public static int powerTotal;
        public static int secondsTotal;
        public static bool checkOpenFile = false;

        //Graphpanes
        GraphPane myPane = new GraphPane(); //heart
        GraphPane myPane2 = new GraphPane(); //speed
        GraphPane myPane3 = new GraphPane(); //altitude
        GraphPane myPane4 = new GraphPane(); //cadence
        GraphPane myPane5 = new GraphPane(); //power
        GraphPane pieChart = new GraphPane(); // all averages
        
        //List collections
        List<int> heartCollection = new List<int>();
        List<int> speedCollection = new List<int>();
        List<int> altitudeCollection = new List<int>();
        List<int> powerCollection = new List<int>();
        List<int> clickCollection = new List<int>();

        //Static variables for max/min
        public static double minHeart = double.MaxValue;
        public static double maxHeart = double.MinValue;
        public static double minSpeed = double.MaxValue;
        public static double maxSpeed = double.MinValue;
        public static double minAltitude = double.MaxValue;
        public static double maxAltitude = double.MinValue;
        public static double minPower = double.MaxValue;
        public static double maxPower = double.MinValue;

        //Static variables for date, time and length of workout
        public static string dateProcessed;
        public static string timeProcessed;
        public static string lengthProcessed;
        public static double workoutLength;
        public static string testTime;

        //Header variables
        public string Chosen_File = "";
        public string version1 = "";
        public string monitor1 = "";
        public string smode1 = "";
        public string date1 = "";
        public string time1 = "";
        public string length1 = "";
        public string interval1 = "";
        public string weight1 = "";

        //HRData variables
        private string HRM;
        private string line;
        private string heart;
        private string speed;
        private string cadence;
        private string altitude;
        private string power;
        private int counter;
        public int seconds;
        
        //DateTime
        DateTime myTime;

        public Form1()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            //Init Variables
            this.Width = 665; //Width of Form
            this.Height = 580; // Height of Form
            labelTitleHeader.Font = new Font(labelTitleHeader.Font.FontFamily, 15); //Sets label title font family and size
            labelTitleStatistics.Font = new Font(labelTitleStatistics.Font.FontFamily, 15); //Sets label title font family and size
            labelTitleHRData.Font = new Font(labelTitleHRData.Font.FontFamily, 15); //Sets label title font family and size
            //Hides panels
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            panel5.Hide();
        }

        //Method to create datagridview columns
        public void dataGridColumns()
        {
            dataGridView1.AllowUserToAddRows = false; //Disallows users to add rows.

            //Datagrideview column for StartTime
            DataGridViewColumn time = new DataGridViewTextBoxColumn();
            time.HeaderText = "StartTime";
            int colIndex1 = dataGridView1.Columns.Add(time);

            //Dategrid view column for heart rate
            DataGridViewColumn heartrate = new DataGridViewTextBoxColumn();
            heartrate.HeaderText = "Heart Rate (bpm)";
            int colIndex2 = dataGridView1.Columns.Add(heartrate);

            //Datagrid view column for speed
            DataGridViewColumn speedkm = new DataGridViewTextBoxColumn();
            speedkm.HeaderText = "Speed (km/h)";
            int colIndex3 = dataGridView1.Columns.Add(speedkm);

            //Datagrid view column for cadencer
            DataGridViewColumn cadence = new DataGridViewTextBoxColumn();
            cadence.HeaderText = "Cadence";
            int colIndex4 = dataGridView1.Columns.Add(cadence);

            //Datagrid view column for altitude
            DataGridViewColumn altitude = new DataGridViewTextBoxColumn();
            altitude.HeaderText = "Altitude";
            int colIndex5 = dataGridView1.Columns.Add(altitude);

            //Datagrid view column for power
            DataGridViewColumn powerwatts = new DataGridViewTextBoxColumn();
            powerwatts.HeaderText = "Power (watts)";
            int colIndex6 = dataGridView1.Columns.Add(powerwatts);
        }

        //Method to open the HRM file
        public void openFile()
        {
            OpenFileDialog openFile = new OpenFileDialog(); //Opens the file selected by the user
            openFile.Title = "Choose File"; //Sets title
            openFile.FileName = ""; //Sets filename
            openFile.Filter = "HRM|*.hrm|Text Document|*.txt"; //Filters HRM files
            if (openFile.ShowDialog() != DialogResult.Cancel) //Checks if the open file dialog is not closed
            {
                Chosen_File = openFile.FileName; //Sets Chosen_File to the chosen file name
                readHeaderData(); //Calls the readHeaderData() method
                readHRData(); //Calls the ReadHRData() method
                toolStripStatusLabel1.Text = "File Loaded"; //Message
            }
        }

        //Method to read Header Data
        public void readHeaderData()
        {
            String[] lines = File.ReadAllLines(Chosen_File); // 

            //Version
            IEnumerable<String> version = from n in lines
                                          where n.StartsWith("Version")
                                          select n.Split('=')[1];
            foreach (String d in version) //
            {
                version1 = d; // 
            }

            //Monitor
            IEnumerable<String> monitor = from n in lines
                                          where n.StartsWith("Monitor")
                                          select n.Split('=')[1];
            foreach (String d in monitor)
            {
                monitor1 = d;
            }

            //SMode
            IEnumerable<String> smode = from n in lines
                                        where n.StartsWith("SMode")
                                        select n.Split('=')[1];
            foreach (String d in smode)
            {
                smode1 = d;
            }

            //Date
            IEnumerable<String> date = from n in lines
                                       where n.StartsWith("Date")
                                       select n.Split('=')[1];
            foreach (String d in date)
            {
                date1 = d;
            }

            //Time
            IEnumerable<String> time = from n in lines
                                       where n.StartsWith("StartTime")
                                       select n.Split('=')[1];
            foreach (String d in time)
            {
                time1 = d;
            }

            //Length
            IEnumerable<String> length = from n in lines
                                         where n.StartsWith("Length")
                                         select n.Split('=')[1];
            foreach (String d in length)
            {
                length1 = d;
            }

            //Interval
            IEnumerable<String> interval = from n in lines
                                           where n.StartsWith("Interval")
                                           select n.Split('=')[1];
            foreach (String d in interval)
            {
                interval1 = d;
            }

            //Weight
            IEnumerable<String> weight = from n in lines
                                         where n.StartsWith("Weight")
                                         select n.Split('=')[1];
            foreach (String d in weight)
            {
                weight1 = d;
            }

            //Date
            string dateString = date1; //Sets dateString
            string dateFormat = "yyyyMMdd"; //Sets a format for date
            DateTime myDate = DateTime.ParseExact(dateString, dateFormat, null); //myDate DateTime (uses dateString, dateFormat)
            dateProcessed = myDate.ToShortDateString(); //Stores myDate as a short date string in dateProcessed

            //Time
            string timeReplaced = time1.Replace(":", string.Empty); //Sets timeReplaced with time1 using replace to remove colons
            decimal timedec = Convert.ToDecimal(timeReplaced); //Can now convert timeReplaced to a decimal timedec
            int timeint = Convert.ToInt32(timedec); //Converts timedec and stores in timeint
            string forTimeAssembly = timeint.ToString(); //Converts timeint into forTimeAsssembly string
            string assembleTime = forTimeAssembly.Insert(2, ":");  //Sets assembleTime to forTimeAssembly and inserts a colon after second character
            string assembledTime = assembleTime.Insert(5, ":"); //Same as before but after the fifth character as well, stores in assembledTime

            testTime = assembledTime;
            string timeFormat = "HH:mm:ss"; //Sets a format for time
            myTime = DateTime.ParseExact(testTime, timeFormat, null); //myTime DateTime (uses timeString, timeFormat)
            timeProcessed = myTime.ToLongTimeString(); //Stores myTime as a long time string in timeProcessed

            //Length
            string lengthReplaced = length1.Replace(":", string.Empty); //Sets lengthReplaced with length1 using replace to remove colons
            decimal lengthdec = Convert.ToDecimal(lengthReplaced); //Can now convert lengthReplaced to a decimal timedec
            int lengthint = Convert.ToInt32(lengthdec); //Converts lengthdec and stores in lengthint
            string testLength = lengthint.ToString();  //Converts lengthint into testLength string
            if (testLength.Length < 6) //Checks if testLength has less than 6 characters
            {
                testLength = lengthint.ToString().PadLeft(6, '0'); //Converts lengthint to string (adding a 0 before the first character using PadLeft)
            }

            string assembleLength = testLength.Insert(2, ":"); //Sets assembleLength to testLength and inserts a colon after second character
            string assembledLength = assembleLength.Insert(5, ":"); //Same as before but after the fifth character as well, stores in assembledLength
            string lengthString = assembledLength; //Sets lengthString to assembledLength 
            DateTime myLength = DateTime.ParseExact(lengthString, timeFormat, null); //myLength DateTime (uses lengthString, timeFormat)
            lengthProcessed = myLength.ToLongTimeString(); //Stores myLength as a long time string in lengthProcessed
            workoutLength = TimeSpan.Parse(lengthProcessed).TotalHours; //Does something... IMPORTANT ;)

            //Display header information in textBox controls
            textBoxVersion.Text = version1;
            textBoxMonitor.Text = monitor1;
            textBoxSMode.Text = smode1;
            textBoxDate.Text = dateProcessed;
            textBoxTime.Text = timeProcessed;
            textBoxLength.Text = lengthProcessed;
            textBoxInterval.Text = interval1;
            textBoxWeight.Text = weight1;
        }

        //Method to read HRData
        public void readHRData()
        {
            myPane = zedGraphControl1.GraphPane; //Heart
            myPane2 = zedGraphControl2.GraphPane; //Speed
            myPane3 = zedGraphControl3.GraphPane; //Altitude
            myPane4 = zedGraphControl4.GraphPane; //Power
            pieChart = zedGraphControl5.GraphPane; //Piechart

            //Set zedgraph titles
            myPane.Title.Text = "Heart Rate";
            myPane2.Title.Text = "Speed";
            myPane3.Title.Text = "Altitude";
            myPane4.Title.Text = "Power";
            pieChart.Title.Text = "All Values";

            
            myPane.XAxis.Title.IsVisible = false;
            myPane.YAxis.Title.IsVisible = false;
            myPane.Y2Axis.Title.IsVisible = false;

            myPane2.XAxis.Title.IsVisible = false;
            myPane2.YAxis.Title.IsVisible = false;
            myPane2.Y2Axis.Title.IsVisible = false;

            myPane3.XAxis.Title.IsVisible = false;
            myPane3.YAxis.Title.IsVisible = false;
            myPane3.Y2Axis.Title.IsVisible = false;

            myPane4.XAxis.Title.IsVisible = false;
            myPane4.YAxis.Title.IsVisible = false;
            myPane4.Y2Axis.Title.IsVisible = false;

            //Instantiate PointPairLists
            PointPairList heartList = new PointPairList();
            PointPairList altitudeList = new PointPairList();
            PointPairList speedList = new PointPairList();
            PointPairList powerList = new PointPairList();

            string path = Chosen_File;  //Sets path to Chosen_File
            using (StreamReader sr = new StreamReader(path)) //Creates new SteamReader using the Chosen_File
            {
                HRM = null; //Sets string HRM to null
                while ((HRM = sr.ReadLine()) != null) //Reads through the file stores each line in HRM
                {
                    if (HRM.IndexOf("[HRData]") != -1) //Checks the string index of HRM for [HRData]
                    {
                        //Finds [HRData]
                        break;
                    }
                }

                line = sr.ReadLine(); //Reads through the file stores each line in line
                double convertSpeed = 0.0; //Double for convertSpeed
                dataGridColumns(); //Calls the dataGridColumns() method
                counter = 0; //Counter is equal to 0
              
                seconds = 1; //makes seconds equal to 1

                while (line != null) //Executes if line is not null
                {
                    counter++; //Increments counter

                    if (seconds <= 1) //Checks if seconds is less than or equal to 1
                    {
                        myTime = myTime.AddSeconds(seconds);//Adds seconds to the date if less than 1
                    }
                    else
                    {
                        if (seconds >= 1) //Checks if the seconds is more than 1
                            myTime = myTime.AddSeconds(seconds); //Adds seconds to the date if more than 1

                    }

                    //Gets the total seconds
                    secondsTotal += seconds;

                    heart = line.Split('\t')[0]; //Splits line string (using tab) and stores characters in heart
                    double heartdouble = Convert.ToDouble(heart);
                    int heartint = Convert.ToInt32(heart); //Converts string heart to integer heartint   
                    heartTotal += heartint; //heartTotal = heartint
                    if (heartint < minHeart) minHeart = heartint; //Finds min value
                    if (heartint > maxHeart) maxHeart = heartint; //Finds max value

                    speed = line.Split('\t')[1]; //Splits line string (using tab) and stores characters in speed
                    double speeddouble = Convert.ToDouble(speed);
                    int speedint = Convert.ToInt32(speed); //Converts string speed to integer speedint
                    convertSpeed = ((double)speedint / 10); //Converts speedint into double convertSpeed and divides by ten (calculates km/h)
                    speedTotal += convertSpeed;  //speedTotal = convertSpeed
                    if (convertSpeed < minSpeed) minSpeed = convertSpeed; //Finds min value
                    if (convertSpeed > maxSpeed) maxSpeed = convertSpeed; //Finds max value

                    cadence = line.Split('\t')[2]; //Splits line string (using tab) and stores characters in cadence
                    double cadencedouble = Convert.ToDouble(cadence);
                    int cadenceint = Convert.ToInt32(cadence); //Converts string cadence to integer cadenceint

                    altitude = line.Split('\t')[3]; //Splits line string (using tab) and stores characters in altitude
                    double altitudedouble = Convert.ToDouble(altitude);
                    int altitudeint = Convert.ToInt32(altitude); //Converts string altitude to integer altitudeint
                    altitudeTotal += altitudeint; //altitudeTotal = altitudeint
                    if (altitudeint < minAltitude) minAltitude = altitudeint; //Finds min value
                    if (altitudeint > maxAltitude) maxAltitude = altitudeint; //Finds max value

                    power = line.Split('\t')[4]; //Splits line string (using tab) and stores characters in power
                    double powerdouble = Convert.ToDouble(power);
                    int powerint = Convert.ToInt32(power); //Converts string power to integer powerint
                    powerTotal += powerint; //powerTotal = powerint
                    if (powerint < minPower) minPower = powerint; //Finds min value
                    if (powerint > maxPower) maxPower = powerint; //Finds max value

                    //String to hold myTime
                    string stringDateTimeStamp = myTime.ToString("HH:mm:ss"); //stores date into string and output it in i.e 02 Feb 14:30:21

                    //Add rows the dataGridView
                    dataGridView1.Rows.Add(stringDateTimeStamp, heartint + " bpm", convertSpeed + " km/h", cadenceint, altitudeint, powerint);

                    line = sr.ReadLine(); //Reads through the file stores each line in line

                    //Data added to PointPair lists to create graphs
                    heartList.Add(new PointPair(counter, heartdouble));
                    speedList.Add(new PointPair(counter, convertSpeed));
                    altitudeList.Add(new PointPair(counter, altitudedouble));
                    powerList.Add(new PointPair(counter, powerdouble));
                }

                //Add data to piechart
                PieItem segment1 = pieChart.AddPieSlice(heartTotal, Color.Navy, Color.White, 45f, 0, "Heart");
                PieItem segment3 = pieChart.AddPieSlice(speedTotal, Color.LimeGreen, Color.White, 45f, 0, "Speed");
                PieItem segment2 = pieChart.AddPieSlice(altitudeTotal, Color.Purple, Color.White, 45f, .0, "Altitude");
                PieItem segment4 = pieChart.AddPieSlice(powerTotal, Color.SandyBrown, Color.White, 45f, 0, "Power");

                //LineItem curves
                LineItem myCurve; //heart
                LineItem myCurve2; //speed
                LineItem myCurve3; //altitude
                LineItem myCurve4; //power

                //Create curve details
                myCurve = myPane.AddCurve("HR [bpm]", heartList, Color.Red, SymbolType.None);
                myCurve2 = myPane2.AddCurve("Speed [km/h]", speedList, Color.Blue, SymbolType.None);
                myCurve3 = myPane3.AddCurve("Altitude [m]", altitudeList, Color.Green, SymbolType.None);
                myCurve4 = myPane4.AddCurve("Power [watts]", powerList, Color.Black, SymbolType.None);

                //Associate this curve with the Y2 axis;
                myCurve.IsY2Axis = true;
                myCurve2.IsY2Axis = true;
                myCurve3.IsY2Axis = true;
                myCurve4.IsY2Axis = true;
        
                myPane.XAxis.MajorGrid.IsVisible = true;
                myPane2.XAxis.MajorGrid.IsVisible = true;
                myPane3.XAxis.MajorGrid.IsVisible = true;
                myPane4.XAxis.MajorGrid.IsVisible = true;
                myPane5.XAxis.MajorGrid.IsVisible = true;

                //Fill the axis background with a gradient
                myPane.Chart.Fill = new Fill(Color.White, Color.Pink, 45.0f);
                myPane2.Chart.Fill = new Fill(Color.White, Color.Pink, 45.0f);
                myPane3.Chart.Fill = new Fill(Color.White, Color.Pink, 45.0f);
                myPane4.Chart.Fill = new Fill(Color.White, Color.Pink, 45.0f);
                myPane5.Chart.Fill = new Fill(Color.White, Color.Pink, 45.0f);

                //Enables scrollbars on zedgraph
                zedGraphControl1.IsShowHScrollBar = true;
                zedGraphControl1.IsShowVScrollBar = true;
                zedGraphControl1.IsAutoScrollRange = true;
                zedGraphControl1.IsScrollY2 = true;

                zedGraphControl2.IsShowHScrollBar = true;
                zedGraphControl2.IsShowVScrollBar = true;
                zedGraphControl2.IsAutoScrollRange = true;
                zedGraphControl2.IsScrollY2 = true;

                zedGraphControl3.IsShowHScrollBar = true;
                zedGraphControl3.IsShowVScrollBar = true;
                zedGraphControl3.IsAutoScrollRange = true;
                zedGraphControl3.IsScrollY2 = true;

                zedGraphControl4.IsShowHScrollBar = true;
                zedGraphControl4.IsShowVScrollBar = true;
                zedGraphControl4.IsAutoScrollRange = true;
                zedGraphControl4.IsScrollY2 = true;

                //Shows tooltips when the mouse hovers over a point
                zedGraphControl1.IsShowPointValues = true;
                zedGraphControl2.IsShowPointValues = true;
                zedGraphControl3.IsShowPointValues = true;
                zedGraphControl4.IsShowPointValues = true;

                zedGraphControl1.PointValueEvent += new ZedGraphControl.PointValueHandler(APointValueHandler);
                zedGraphControl2.PointValueEvent += new ZedGraphControl.PointValueHandler(APointValueHandler);
                zedGraphControl3.PointValueEvent += new ZedGraphControl.PointValueHandler(APointValueHandler);
                zedGraphControl4.PointValueEvent += new ZedGraphControl.PointValueHandler(APointValueHandler);
 
                //Axis change
                zedGraphControl1.AxisChange();
                zedGraphControl2.AxisChange();
                zedGraphControl3.AxisChange();
                zedGraphControl4.AxisChange();
                zedGraphControl5.AxisChange();

                //Invalidate is used to re-draw the graph
                zedGraphControl1.Invalidate();
                zedGraphControl2.Invalidate();
                zedGraphControl3.Invalidate();
                zedGraphControl4.Invalidate();
       
                checkOpenFile = true; //Sets checkOpenFile bool to true
                outputStatistics(); //Calls the outputStatistics method
              
            }
        }

        //String to return the values of the point under the mouse on the zedGraph
        private string APointValueHandler(ZedGraphControl control, GraphPane pane,
                        CurveItem curve, int curveint)
        {
            //Gets the PointPair under the mouse
            PointPair pt = curve[curveint];    
            //Returns as text string
            return curve.Label.Text + " is " + pt.Y.ToString("f0") + " units at " + pt.X.ToString("f1") + " counter";
        }

        //Method to calculate user values for heart, uses the calc class
        public void calculateHeart()
        {
            //Instantiates the calc class
            Calc calc = new Calc();
            double heartAvg = 0.0;
            double heartMax = 0.0;
            double heartMin = 0.0;

            //Uses the calc class
            heartAvg = calc.calculateAverage(heartCollection);
            heartMin = calc.calculateMinimum(heartCollection);
            heartMax = calc.calculateMaximum(heartCollection);

            //Output
            textBoxUserHeartAverage.Text = heartAvg.ToString() + " bpm";
            textBoxUserHeartMax.Text = heartMax.ToString() + " bpm";
            textBoxUserHeartMin.Text = heartMin.ToString() + " bpm";
        }

        //Method to calculate user values for speed, uses the calc class
        public void calculateSpeed()
        {
            //Instantiates the calc class
            Calc calc = new Calc();
            double speedAvg = 0.0;
            double speedMax = 0.0;
            double speedMin = 0.0;

            //Uses the calc class
            speedAvg = calc.calculateAverage(speedCollection);
            speedMin = calc.calculateMinimum(speedCollection);
            speedMax = calc.calculateMaximum(speedCollection);

            //Output
            textBoxUserSpeedAverage.Text = speedAvg.ToString() + " Km/h";
            textBoxUserSpeedMax.Text = speedMax.ToString() + " Km/h";
            textBoxUserSpeedMin.Text = speedMin.ToString() + " Km/h";
        }

        //Method to calculate user values for altitude, uses the calc class
        public void calculateAltitude()
        {
            //Instantiates the calc class
            Calc calc = new Calc();
            double altitudeAvg = 0.0;
            double altitudeMax = 0.0;
            double altitudeMin = 0.0;

            //Uses the calc class
            altitudeAvg = calc.calculateAverage(altitudeCollection);
            altitudeMin = calc.calculateMinimum(altitudeCollection);
            altitudeMax = calc.calculateMaximum(altitudeCollection);

            //Output
            textBoxUserAltitudeAverage.Text = altitudeAvg.ToString() + "0";
            textBoxUserAltitudeMax.Text = altitudeMax.ToString() + "0";
            textBoxUserAltitudeMin.Text = altitudeMin.ToString() + "0";
        }

        //Method to calculate user values for power, uses the calc class
        public void calculatePower()
        {
            //Instantiates the calc class
            Calc calc = new Calc();
            double powerAvg = 0.0;
            double powerMax = 0.0;
            double powerMin = 0.0;

            //Uses the calc class
            powerAvg = calc.calculateAverage(powerCollection);
            powerMin = calc.calculateMinimum(powerCollection);
            powerMax = calc.calculateMaximum(powerCollection);

            //Output
            textBoxUserPowerAverage.Text = powerAvg.ToString() + " watts";
            textBoxUserPowerMax.Text = powerMax.ToString() + " watts";
            textBoxUserPowerMin.Text = powerMin.ToString() + " watts";
        }

        //Method to output statistics
        public void outputStatistics()
        {
            //Set double variables for averages
            double speedAvg = 0.0;
            double altitudeAvg = 0.0;
            double heartAvg = 0.0;
            double powerAvg = 0.0;
            double totalDistance = 0.0;

            if (counter > 0) //Checks if counter is more than 0
            {
                speedAvg = speedTotal / counter; //speedAvg = speedTotal divided by counter
                heartAvg = heartTotal / counter; //heartAvg = heartTotal divided by counter
                altitudeAvg = altitudeTotal / counter; //altitudeAvg = altitudeTotal divided by counter
                powerAvg = powerTotal / counter; //powerAvg = powerTotal divided by counter
                totalDistance = workoutLength * speedAvg; //totalDistance = workoutLength multiplied by speedAvg
            }

            //Instantiates the calc class
            Calc calc = new Calc();

            //Double for NP, uses the calc class to caculate NP (Normalized Power)
            double NP = calc.calculateNP(minPower, powerAvg, maxPower);

            //Display for speed statistics
            textBoxAverageSpeed.Text = speedAvg.ToString("0.0") + " km/h";
            textBoxMinSpeed.Text = minSpeed.ToString("0.0") + " km/h";
            textBoxMaxSpeed.Text = maxSpeed.ToString("0.0") + " km/h";

            //Display for heart statistics
            textBoxAverageHeart.Text = heartAvg.ToString() + " bpm";
            textBoxMinHeart.Text = minHeart.ToString() + " bpm";
            textBoxMaxHeart.Text = maxHeart.ToString() + " bpm";

            //Display for altitude statistics
            textBoxAverageAltitude.Text = altitudeAvg.ToString();
            textBoxMinAltitude.Text = minAltitude.ToString();
            textBoxMaxAltitude.Text = maxAltitude.ToString();

            //Display for power statistics
            textBoxAveragePower.Text = powerAvg.ToString();
            textBoxMinPower.Text = minPower.ToString();
            textBoxMaxPower.Text = maxPower.ToString();

            //Display for total distance statistic
            textBoxTotalDistance.Text = totalDistance.ToString("0.0") + " km";

            //Displays NP (Normalized power)
            textBoxNP.Text = NP.ToString(); //Outputs NP
        }

        //Method to clear static totals
        public void clearStaticTotals()
        {
            //Sets static totals variables to 0
            speedTotal = 0.0;
            heartTotal = 0;
            altitudeTotal = 0;
            powerTotal = 0;
        }

        //Method to clear contents of controls
        public void clearContents()
        {
            checkOpenFile = false;
            //zedGraphControl1.GraphPane.CurveList.Clear();
           // zedGraphControl1.Refresh();
            clearStaticTotals(); //Calls the clearStaticTotals() method

            //Clears header data textbox controls
            textBoxVersion.Text = textBoxMonitor.Text = textBoxSMode.Text = textBoxDate.Text = textBoxTime.Text = textBoxLength.Text = textBoxInterval.Text
            = textBoxWeight.Text = "";

            //Clears speed statistics controls
            textBoxAverageSpeed.Text = "";
            textBoxMinSpeed.Text = "";
            textBoxMaxSpeed.Text = "";

            //Clears heart rate statistics controls
            textBoxAverageHeart.Text = "";
            textBoxMinHeart.Text = "";
            textBoxMaxHeart.Text = "";

            //Clears altitude statistics controls
            textBoxAverageAltitude.Text = "";
            textBoxMinAltitude.Text = "";
            textBoxMaxAltitude.Text = "";

            //Clears power statistics controls
            textBoxAveragePower.Text = "";
            textBoxMinPower.Text = "";
            textBoxMaxPower.Text = "";

            //Clears totla distance statistic control
            textBoxTotalDistance.Text = "";

            //Clears dataGridView1 contents

            do //Start of do while loop
            {
                foreach (DataGridViewRow row in dataGridView1.Rows) //Iterates dataGridView1.Rows as rows
                {
                    try //Start of try catch loop
                    {
                        dataGridView1.Rows.Remove(row); //Removes rows from dataGridView1
                    }
                    catch (Exception) { } //Catches any exceptions
                }
            } while (dataGridView1.Rows.Count >= 1); //While dataGridView1 rows are more or equal to 1
        }

        //Heart graph method to double click to select points of data and store in list array
        private void zedGraphControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = 0;
            object nearestobject = null;
            PointF clickedPoint = new PointF(e.X, e.Y);
            zedGraphControl1.GraphPane.FindNearestObject(clickedPoint, this.CreateGraphics(), out
            nearestobject, out index);
            double x;
            double y;
            PointF mousePt = new PointF(e.X, e.Y);
            myPane.ReverseTransform(mousePt, out x, out y); //Uses the reverse transorm method
            int outputY = Convert.ToInt32(y);
            heartCollection.Add(outputY);
        }

        //Speed graph method to double click to select points of data and store in list array
        private void zedGraphControl2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = 0;
            object nearestobject = null;
            PointF clickedPoint = new PointF(e.X, e.Y);
            zedGraphControl2.GraphPane.FindNearestObject(clickedPoint, this.CreateGraphics(), out
            nearestobject, out index);
            double x;
            double y;
            PointF mousePt = new PointF(e.X, e.Y);
            myPane2.ReverseTransform(mousePt, out x, out y); //Uses the reverse transorm method
            int outputY = Convert.ToInt32(y);
            speedCollection.Add(outputY);
        }

        //Altitude graph method to double click to select points of data and store in list array
        private void zedGraphControl3_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = 0;
            object nearestobject = null;
            PointF clickedPoint = new PointF(e.X, e.Y);
            zedGraphControl3.GraphPane.FindNearestObject(clickedPoint, this.CreateGraphics(), out
            nearestobject, out index);
            double x;
            double y;
            PointF mousePt = new PointF(e.X, e.Y);
            myPane2.ReverseTransform(mousePt, out x, out y); //Uses the reverse transorm method
            int outputY = Convert.ToInt32(y);
            altitudeCollection.Add(outputY);
        }
        //Power graph method to double click to select points of data and store in list array
        private void zedGraphControl4_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = 0;
            object nearestobject = null;
            PointF clickedPoint = new PointF(e.X, e.Y);
            zedGraphControl4.GraphPane.FindNearestObject(clickedPoint, this.CreateGraphics(), out
            nearestobject, out index);
            double x;
            double y;
            PointF mousePt = new PointF(e.X, e.Y);
            myPane2.ReverseTransform(mousePt, out x, out y); //Uses the reverse transorm method
            int outputY = Convert.ToInt32(y);
            powerCollection.Add(outputY);
        }

        //Select file menu item
        private void selectFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFile(); //Calls the openFile() method
            if (checkOpenFile == true)
            {
                selectFileToolStripMenuItem.Enabled = false;
               
            }
            if (checkOpenFile == false)
            {
                resetToolStripMenuItem1.Enabled = false;
            }
        }

        //Reset menu item
        private void resetToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            clearContents(); //Calls the clearContents method
            toolStripStatusLabel1.Text = "Contents Cleared"; //Message
            if (checkOpenFile == false)
            {
                selectFileToolStripMenuItem.Enabled = true;
            }
        }

        //About menu item
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "About: Program to read data from HRM file and output results - Created by Mark Ellis : c3374267";
        }

        //Heart graph menu item
        private void heartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Show();
            panel1.BringToFront();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            heartCollection.Clear(); //Clears list array
            speedCollection.Clear(); //Clears list array
            altitudeCollection.Clear(); //Clears list array
            powerCollection.Clear(); //Clears list array
        }

        //Speed graph menu item
        private void speedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel2.Show();
            panel2.BringToFront();
            panel1.Hide();
            panel3.Hide();
            panel4.Hide();
            heartCollection.Clear(); //Clears list array
            speedCollection.Clear(); //Clears list array
            altitudeCollection.Clear(); //Clears list array
            powerCollection.Clear(); //Clears list array
        }

        //Altitude graph menu item
        private void altitudeToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            panel3.Show();
            panel3.BringToFront();
            panel1.Hide();
            panel2.Hide();
            panel4.Hide();
            heartCollection.Clear(); //Clears list array
            speedCollection.Clear(); //Clears list array
            altitudeCollection.Clear(); //Clears list array
            powerCollection.Clear(); //Clears list array
        }

        //Power graph menu item
        private void powerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel4.Show();
            panel4.BringToFront();
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            heartCollection.Clear(); //Clears list array
            speedCollection.Clear(); //Clears list array
            altitudeCollection.Clear(); //Clears list array
            powerCollection.Clear(); //Clears list array
        }

        //Pie chart menu item
        private void pieChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel5.Show();
            panel5.BringToFront();
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
        }

        //Panel 1 back button
        private void button1_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            panel5.Hide();
        }

        //Panel 2 back button
        private void button2_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            panel5.Hide();
        }

        //Panel 3 back button
        private void button3_Click_1(object sender, EventArgs e)
        {
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            panel5.Hide();
        }

        //Panel 4 back button
        private void Panel4Back_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            panel5.Hide();
        }

        //Panel 5 back button
        private void Panel5Back_Click(object sender, EventArgs e)
        {
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
            panel4.Hide();
            panel5.Hide();
        }

        //Button to calculate user heart values
        private void button6_Click(object sender, EventArgs e)
        {
            calculateHeart(); //Calls the method
        }

        //Button to calculate user speed values
        private void button5_Click(object sender, EventArgs e)
        {
            calculateSpeed(); //Calls the method
        }

        //Button to calculate user altitude values
        private void Panel3Create_Click(object sender, EventArgs e)
        {
            calculateAltitude(); //Calls the method
        }

        //Button to calculate user power values
        private void Panel4Create_Click(object sender, EventArgs e)
        {
            calculatePower(); //Calls the method
        }

        //Exit menu item
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit(); //Exits application
        } 
  }
}

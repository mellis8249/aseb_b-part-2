﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEB_2
{
    public class Calc
    {
        //Method to calculate the average number in the list array
        public double calculateAverage(List<int> Data)
        {
            //Calculates the average
            double calcAverage = Data.Average();
            calcAverage = Math.Round(calcAverage, 0);
            //Returns the average 
            return calcAverage;
        }

        //Method to calculate the maximum number in the list array
        public double calculateMaximum(List<int> Data)
        {
            //Calculates the highest number in the list
            double calcMaximum = Data.Max();

            //Returns the highest number in the list
            return calcMaximum;
        }

        //Method to calculate the minimum number in the list array
        public double calculateMinimum(List<int> Data)
        {
            //Calculates the lowest number in the list
            int calcMinimum = Data.Min();

            //Returns the lowest number in the list
            return calcMinimum;
        }

        //Method to calculate NP by rasining each double to the 4th power, adding these values together dividing them by three
        //and rounding this value with the method to find the nth root
        public double calculateNP(double one, double two, double three)
        {
            double first = Math.Pow(one, 4);
            double second = Math.Pow(two, 4);
            double third = Math.Pow(three, 4);

            double NP = (first + second + third) / 3;
            double answer = Math.Round(NthRoot(NP, 4));
            return answer;
        }

        //Method to find the nth root.  
        public double NthRoot(double A, int N)
        {
            return Math.Pow(A, 1.0 / N);
        }
    }
}
